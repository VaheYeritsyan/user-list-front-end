import * as React from 'react';
import {useContext, useState} from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField'
import {createUseStyles} from 'react-jss';
import All from './context/All';
import {useNavigate} from 'react-router-dom';
import {PRODUCTS} from './constant/constant';

const useStyles = createUseStyles({
    dialog: {
        margin: [0, 'auto']
    }, title: {
        textAlign: 'center'
    }, dialogActions: {
        width: 600, display: 'flex', flexDirection: 'column', gap: 10,
    },

})

export default function CreateProduct() {
    const classes = useStyles()
    const createUser = useContext(All)
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const navigate = useNavigate('')

    const addNewUser = async () => {
        const newUser = {
            first_name: firstName, last_name: lastName,
        };

        try {
            const response = await fetch('http://localhost:8080/api/users', {
                method: 'POST', headers: {
                    'Content-Type': 'application/json',
                }, body: JSON.stringify(newUser),
            });

            if (!response.ok) {
                throw new Error(`Error: ${response.status}`);
            }

            const responseData = await response.json();
            console.log('User created successfully:', responseData);

            setFirstName('');
            setLastName('');
            navigate(`/${PRODUCTS}`);
        } catch (error) {
            console.error('Error creating user:', error);
        }
    };


    return (<Dialog
            className={classes.dialog}
            open={true}
        >
            <Button onClick={() => {
                return navigate(-1)
            }}> Go Back </Button>
            <DialogTitle className={classes.title}> Create new user</DialogTitle>
            <DialogActions className={classes.dialogActions}>
                <TextField placeholder='First name' value={firstName} onChange={(e) => {
                    setFirstName(e.target.value)
                }}/>
                <TextField placeholder='Last name' value={lastName} onChange={(e) => {
                    setLastName(e.target.value)
                }}/>
                <Button disabled={!firstName || !lastName} variant='outlined' onClick={addNewUser}> Create </Button>
            </DialogActions>
        </Dialog>)
}
