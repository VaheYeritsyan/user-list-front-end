import React, { useState, useContext } from 'react'
import TablePagination from '@mui/material/TablePagination';
import All from './context/All';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableContainer from '@mui/material/TableContainer';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import Button from '@mui/material/Button'
import Delete from './Delete';

export default function ShowUserList() {
    const allProducts = useContext(All)
    const [showDelete, setShowDelete] = useState(false)
    const [deletedItem, setDeletedItem] = useState({})
    const [page, setPage] = useState(0)
    const [rowsPerPage, setRowsPerPage] = useState(5)
    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value), 10)
        setPage(0)
    }
    const rowsToShow = rowsPerPage === -1 ? allProducts.user : allProducts.user.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)

    const onDelete = (e) => {
      allProducts.setUser(() => {
        return allProducts.user.filter((item) => item.id !== e.id)
      })
    }
  return (
    <div>
         <TableContainer component={'div'}>
      <Table sx={{ width: 700, margin: [0, 'auto'] }} aria-label="custom pagination table">
        <TableBody>
{rowsToShow.map((item) => {
    return <TableRow key={item.serialNumber}>
    <TableCell component="th" scope="row">
      {item.firstName}
    </TableCell>
    <TableCell style={{ width: 160 }} align="left">
      {item.lastName}
    </TableCell>
    <Button onClick={() => {
     setShowDelete(true)
      setDeletedItem(item)
    }}>  Delete </Button>
  </TableRow>
})}
        </TableBody>
        <TablePagination
            component='div'
            rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
            count={allProducts.user.length}
            page={page}
            rowsPerPage={rowsPerPage}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
        />
</Table>
</TableContainer>
{showDelete && <Delete onDelete={onDelete} deletedItem={deletedItem} item={deletedItem.firstName} setShowDelete={setShowDelete} />}
    </div>
  )
}
