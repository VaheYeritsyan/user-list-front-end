import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';


export default function Delete(props) {
    const {setShowDelete, deletedItem, item, onDelete} = props;
   
  return (
    <Dialog
    open={true}
    onClose={() => {setShowDelete(false)}}
  >
     
    <DialogTitle> Delete </DialogTitle>
    <DialogContent> Are you sure you want to delete '{item}' </DialogContent>
    <DialogActions>
    <Button  variant='contained' color='error' onClick={() => {
        onDelete(deletedItem)
        setShowDelete(false)
    }}> Agree </Button>
    <Button  variant='outlined' onClick={() => {setShowDelete(false)}}> Cancel </Button>
    </DialogActions>
    
  </Dialog>
  )
}
