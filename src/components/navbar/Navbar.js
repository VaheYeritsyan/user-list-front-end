import React from 'react'
import Box from '@mui/material/Box'
import { Link, Route, Routes } from 'react-router-dom'
import { createUseStyles } from 'react-jss'
import CreateUser from './../CreateUser'
import CreateProduct from './CreateProduct'
import ShowUserList from '../ShowUserList'
import ShowProductsList from '../ShowProductList'
import { CREATEPRODUCT, CREATEUSER, PRODUCTS, USERS } from '../constant/constant'

const useStyles = createUseStyles({
    box: {
        height: 50,
        backgroundColor: 'blue',
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        gap: 5,

    },
    Link: {
        width: 190,
        color: 'white',
        textDecoration: 'none',
      
        '&:hover': {
            color: 'red'
        }
    },
})

export default function Navbar() {
    const classes = useStyles()
  return (
    <div>
        <Box className={classes.box}>
            <Link to={CREATEUSER} className={classes.Link}> Create User </Link>
            <Link to={CREATEPRODUCT}  className={classes.Link}> Create Product </Link>
            <Link  to={PRODUCTS} className={classes.Link}> Show Product List </Link>
            <Link to={USERS} className={classes.Link}> Show User List </Link>
        </Box>
        <Routes> 
            <Route path={CREATEUSER} element={<CreateUser />}> Create User</Route>
            <Route path={CREATEPRODUCT} element={<CreateProduct />}> Create Product </Route>
            <Route path={PRODUCTS} element={<ShowProductsList />}> All Products </Route>
           <Route path={USERS} element={<ShowUserList />}> Show User List </Route>
        </Routes>
    </div>
  )
}
