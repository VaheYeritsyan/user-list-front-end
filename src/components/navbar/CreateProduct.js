import * as React from 'react';
import { useState, useContext } from 'react'
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField'
import { createUseStyles } from 'react-jss';
import All from '../context/All';
import { v4 as uuid } from 'uuid'
import { useNavigate } from 'react-router-dom';

const useStyles = createUseStyles({
    dialog: {
        margin: [0, 'auto']
    },
    title: {
        textAlign: 'center'
    },
    dialogActions: {
        width: 600,
        display: 'flex',
        flexDirection: 'column',
        gap: 10,
    },
   
})
export default function CreateProduct() {
    const classes = useStyles()
    const createProduct = useContext(All)
    const [newProduct, setNewProduct] = useState('')
    const navigate = useNavigate()

    const addNewProduct = () => {
        createProduct.setProduct(() => {
            return [
                ...createProduct.product,
                {
                    product: newProduct,
                    serialNumber: uuid(),
                }
            ]
        })
        setNewProduct('')
    }

    return (
        <Dialog
            className={classes.dialog}
            open={true}
        >
            <Button onClick={() => {
                navigate(-1)
            }}> Go back </Button>
            <DialogTitle className={classes.title}> Create new Product</DialogTitle>
            <DialogActions className={classes.dialogActions}>

            <TextField placeholder='Product' value={newProduct} onChange={(e) => {setNewProduct(e.target.value)}} />
            <Button variant='outlined' disabled={!newProduct} onClick={addNewProduct}> Create Product </Button>
            </DialogActions>
        </Dialog>
  )
}
