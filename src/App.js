import { useState } from 'react'
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Navbar from "./components/navbar/Navbar";
import All from "./components/context/All";

function App() {
  const [user, setUser] = useState([])
  const [product, setProduct] = useState([])

  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path='*' element={
          <All.Provider value={{user: user, setUser: setUser, product: product, setProduct: setProduct}}> <Navbar /> </All.Provider>
          }> Navbar </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
